plugins {
    kotlin("jvm")
    id("io.spring.dependency-management")
    application
}

repositories {
    jcenter()
}

group = "com.dalewking"
version = "1.0-SNAPSHOT"

application {
    mainClassName = "EmailExtractor"
}

val run: JavaExec by tasks
run.standardInput = System.`in`

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("com.sun.mail:javax.mail")
    implementation("org.camunda.connect:camunda-connect-core")
    implementation("org.camunda.bpm.extension:camunda-bpm-mail-core")
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
}


