@file:JvmName("EmailExtractor")

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.camunda.bpm.extension.mail.MailConnectors
import org.camunda.bpm.extension.mail.config.MailConfigurationFactory
import org.camunda.bpm.extension.mail.config.PropertiesMailConfiguration
import org.camunda.bpm.extension.mail.poll.PollMailConnector

fun main() {
    val pollMails = MailConnectors.pollMails()
    val propertiesMailConfiguration = MailConfigurationFactory.getConfiguration() as PropertiesMailConfiguration
//    val properties = propertiesMailConfiguration.properties.setProperty("mail.password", readPassword.toString());

    val response = pollMails.createRequest()
            .folder("INBOX")
            .downloadAttachments(false)
            .execute()
            .mails

    val mapper = ObjectMapper(YAMLFactory())
            .registerModule(KotlinModule())

    mapper.writeValue(System.out, response)
}