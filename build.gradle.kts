plugins {
    kotlin("jvm") version "1.3.60"
    id("org.openapi.generator") version("4.2.1")
    id("io.spring.dependency-management") version("1.0.8.RELEASE")
    application
}

repositories {
    jcenter()
}

group = "com.dalewking"
version = "1.0-SNAPSHOT"

application {
    mainClassName = "com.nobleworks_software.gmail_handler.Main"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("org.jsoup:jsoup")

    implementation("com.sun.mail:javax.mail")
    implementation("com.github.salomonbrys.kodein:kodein")
    implementation("space.traversal.kapsule:kapsule-core")
    implementation("com.github.debop:koda-time")
    implementation("com.github.kittinunf.fuel:fuel")
    implementation("com.github.kittinunf.fuel:fuel-moshi")
    implementation("com.squareup.moshi:moshi-kotlin")
    implementation("com.squareup.moshi:moshi-adapters")
    implementation("com.squareup.okhttp3:okhttp")
    implementation("io.swagger:swagger-annotations")
    implementation("javax.annotation:javax.annotation-api")
    implementation("com.google.code.findbugs:jsr305")
    implementation("com.squareup.okhttp3:okhttp")
    implementation("com.squareup.okhttp3:logging-interceptor")
    implementation("com.google.code.gson:gson")
    implementation("org.apache.oltu.oauth2:org.apache.oltu.oauth2.client")
    implementation("io.gsonfire:gson-fire")
}

allprojects {
    apply(plugin = "io.spring.dependency-management")

    dependencyManagement {
        dependencies {
            dependency("org.jsoup:jsoup:1.12.1")
            dependency("com.sun.mail:javax.mail:1.6.0")
            dependency("org.camunda.connect:camunda-connect-core:1.3.0")
            dependency("org.camunda.bpm.extension:camunda-bpm-mail-core:1.2.0")

            dependency("com.github.salomonbrys.kodein:kodein:4.1.0")
            dependency("space.traversal.kapsule:kapsule-core:0.3")
            dependency("com.github.debop:koda-time:1.2.2")
            dependency("com.github.kittinunf.fuel:fuel:1.16.0")
            dependency("com.github.kittinunf.fuel:fuel-moshi:1.16.0")
            dependency("com.squareup.moshi:moshi-kotlin:1.8.0")
            dependency("com.squareup.moshi:moshi-adapters:1.8.0")
            dependency("com.squareup.okhttp3:okhttp:3.8.0")
            dependency("io.swagger:swagger-annotations:1.5.17")
            dependency("javax.annotation:javax.annotation-api:1.3.2")
            dependency("com.google.code.findbugs:jsr305:3.0.2")
            dependencySet("com.squareup.okhttp3:4.2.2") {
                entry("okhttp")
                entry("logging-interceptor")
            }
            dependency("com.google.code.gson:gson:2.8.1")
            dependency("org.apache.oltu.oauth2:org.apache.oltu.oauth2.client:1.0.1")
            dependency("io.gsonfire:gson-fire:1.8.0")
            dependency("com.fasterxml.jackson.core:jackson-databind:2.7.1-1")
            dependency("com.fasterxml.jackson.module:jackson-module-kotlin:2.7.1-2")
            dependency("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.7.1")
        }
    }
}

sourceSets.main {
    java.srcDir("${project.buildDir.path}/swagger/src/main/kotlin")
}

tasks {
    compileKotlin {
        dependsOn(openApiGenerate)
        kotlinOptions.jvmTarget = "1.8"
    }

    val swaggerInput = "ynab-v1-swagger.json"
    val swaggerOutputDir = file("build/swagger")

    val fixSwagger by registering(Copy::class) {
        from(swaggerInput)
        into("$buildDir/generated/swagger-fix")
        filter { it.replace("\"name\": \"data\"", "\"name\": \"the_data\"") }
    }

    openApiGenerate {
        inputSpec.set("${fixSwagger.get().outputs.files.singleFile}/$swaggerInput")
        outputDir.set(swaggerOutputDir.toString())
        generatorName.set("kotlin")
        generateModelDocumentation.set(false)
        packageName.set("com.nobleworks_software.ynab.client")
        invokerPackage.set("com.nobleworks_software.ynab.client")
        modelPackage.set("com.nobleworks_software.ynab.client.model")
        apiPackage.set("com.nobleworks_software.ynab.client.api")
        configOptions.put("dateLibrary", "java8")
        apiFilesConstrainedTo.set(listOf("Transactions"))
        modelFilesConstrainedTo.set(listOf(
                "TransactionsResponse_data",
                "ErrorResponse",
                "ErrorDetail",
                "HybridTransactionsResponse_data",
                "HybridTransactionsResponse",
                "HybridTransaction",
                "HybridTransaction_allOf",
                "SaveTransaction",
                "SaveTransactionWrapper",
                "SaveTransactionsResponse_data",
                "SaveTransactionsResponse",
                "SaveTransactionsWrapper",
                "SubTransaction",
                "TransactionResponse_data",
                "TransactionResponse",
                "TransactionDetail",
                "TransactionDetail_allOf",
                "TransactionSummary",
                "TransactionsResponse",
                "UpdateTransaction",
                "UpdateTransaction_allOf",
                "UpdateTransactionsWrapper"
        ))
        supportingFilesConstrainedTo.set(listOf(
                "ApiClient.kt",
                "ApiAbstractions.kt",
                "ApiInfrastructureResponse.kt",
                "ByteArrayAdapter.kt",
                "Errors.kt",
                "LocalDateAdapter.kt",
                "LocalDateTimeAdapter.kt",
                "RequestConfig.kt",
                "RequestMethod.kt",
                "ResponseExtensions.kt",
                "ResponseType.kt",
                "Serializer.kt",
                "UUIDAdapter.kt"
        ))
    }

    "openApiGenerate" {
        dependsOn(fixSwagger)
    }

    build { dependsOn(installDist) }
    register("stage") { dependsOn(build) }

    clean {
        doFirst {
            delete(swaggerOutputDir)
        }
    }
}


