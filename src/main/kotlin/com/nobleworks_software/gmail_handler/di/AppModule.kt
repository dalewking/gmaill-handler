package com.nobleworks_software.gmail_handler.di

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.nobleworks_software.gmail_handler.TaskBuilder
import com.nobleworks_software.ynab.client.api.TransactionsApi
import com.nobleworks_software.ynab.client.infrastructure.ApiClient
import com.nobleworks_software.ynab.client.model.SaveTransaction
import com.nobleworks_software.ynab.client.model.SaveTransactionsWrapper
import com.squareup.moshi.JsonReader
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okio.buffer
import okio.source
import java.time.Duration
import java.time.Instant
import java.time.ZoneOffset
import java.util.*
import javax.mail.Folder
import javax.mail.Message
import javax.mail.Session

class AppModule
{
    private val gmailPassword = System.getenv("GMAIL_PASSWORD")

    private val imapProperties = Properties().apply { put("mail.imap.ssl.enable", "true") }
    private val imapSession = Session.getInstance(imapProperties)

    private val nirvanaUrl = "https://api.nirvanahq.com/"
    private val gmailAddress = System.getenv("GMAIL_ADDRESS")
    private val nirvanaUser = System.getenv("NIRVANA_EMAIL")
    private val nirvanaPassword = System.getenv("NIRVANA_PASSWORD_HASH")
    private val ynabToken = System.getenv("YNAB_TOKEN")
    private val ynabBudget = "0ced8c79-93e8-4502-a6c0-dd0465c4a19d"
    private val moshi = Moshi.Builder().build()

    private val messagesStore by lazy()
    {
        imapSession.getStore("imap")
                .apply { connect("imap.gmail.com", gmailAddress, gmailPassword) }
    }

    private val inbox by lazy { messagesStore.getFolder("Inbox").apply { open(Folder.READ_ONLY) } }

    private val trash by lazy { messagesStore.getFolder("[Gmail]/Trash") }

    val messageSource: () -> Array<Message> = { inbox.messages }

    val messageDeleter: List<Message>.() -> Unit = { inbox.copyMessages(toTypedArray(), trash) }

    @Suppress("UNCHECKED_CAST")
    fun sendTasksToNirvana(tasks: List<Map<String, Any?>>)
    {
        val gmtoffset = Duration.ofSeconds(ZoneOffset.systemDefault().rules.getOffset(Instant.now()).totalSeconds.toLong())
            .toHours()

        val authToken = getAuthToken(gmtoffset)

        val json = moshi.adapter<List<Map<String, Any?>>>(
                Types.newParameterizedType(List::class.javaObjectType, Map::class.javaObjectType)).toJson(tasks)

        nirvanaUrl
                .httpGet(listOf(
                        "appid" to "nirvanahqjs",
                        "requestid" to UUID.randomUUID().toString(),
                        "clienttime" to Instant.now().epochSecond.toString(),
                        "appversion" to "0",
                        "api" to "json",
                        "authtoken" to authToken,
                        "gmtoffset" to gmtoffset)).path
                .httpPost(listOf())
                .header("content-type" to "application/json")
                .jsonBody(json)
                .response()
                .second
    }

    private fun getAuthToken(gmtoffset: Long): String
    {
        return nirvanaUrl
        .httpGet(listOf(
                "appid" to "nirvanahqjs",
                "requestid" to UUID.randomUUID().toString(),
                "clienttime" to Instant.now().epochSecond.toString(),
                "appversion" to "0",
                "api" to "rest")).path
        .httpPost(listOf(
                "u" to nirvanaUser,
                "p" to nirvanaPassword,
                "method" to "auth.new",
                "gmtoffset" to gmtoffset))
        .response()
        .second
        .run { data.inputStream().source().buffer() }
                .let { JsonReader.of(it) }
                .let { it.readJsonValue() ?: throw IllegalStateException() }
                .let { it as Map<String, Any?> }
                .let { it["results"] as List<Map<String, Any?>> }
                .let { it.asSequence() }
                .let { it.filter { it.containsKey("auth") } }
                .map { it["auth"] as Map<String, Any?> }
                .map { it["token"].toString() }
                .let { it.first() }
    }

    val taskCreator: (List<TaskBuilder>) -> Unit =
            { tasks ->
                sendTasksToNirvana(tasks.asSequence()
                    .onEach { println("Creating task $it") }
                    .map { it.toNirvanaTask() }
                    .toList())
            }

    val transactionCreator: (List<SaveTransaction>) -> Unit =
            {
                if(it.isNotEmpty())
                {
                    ApiClient.apiKeyPrefix["Authorization"] = "Bearer"
                    ApiClient.apiKey["Authorization"] = ynabToken
                    TransactionsApi()
                            .createTransaction(
                                ynabBudget,
                                SaveTransactionsWrapper(transactions = it.toTypedArray()))
                }
            }
}