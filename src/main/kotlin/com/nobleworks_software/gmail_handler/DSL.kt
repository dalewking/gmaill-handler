package com.nobleworks_software.gmail_handler

import com.nobleworks_software.ynab.client.model.SaveTransaction
import java.time.LocalDate
import java.time.LocalDateTime
import javax.mail.Message

interface DSL
{
    val message: Message

    val msgDate: LocalDate

    val msgDateTime: LocalDateTime

    val month: String

    val year: String

    val messageText: String

    fun DSL?.hasSubject(regex: String): DSL?

    fun DSL?.isFrom(regex: String): DSL?

    fun DSL?.bodyContains(regex: String): DSL?

    fun DSL?.makeTask(config: TaskBuilder.() -> Unit): DSL?

    fun DSL?.makeBudgetTransaction(transactionBuilder: DSL.() -> SaveTransaction?): DSL?

    fun DSL?.deleteIt(): DSL?
}