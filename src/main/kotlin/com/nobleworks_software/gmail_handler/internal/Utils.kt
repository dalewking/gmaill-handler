package com.nobleworks_software.gmail_handler.internal

import javax.mail.Multipart
import javax.mail.Part

fun Part.getText(allowHtml: Boolean = false): String?
        = when
        {
            allowHtml && isMimeType("text/html") -> content as String
            isMimeType("text/*") -> content as String
            content is Multipart -> parts.mapNotNull { it.getText(allowHtml) }.firstOrNull()
            else -> null
        }

private val Part.parts
    get() = (this.content as? Multipart)?.run { (0 until count).map { getBodyPart(it) } } ?: listOf()

fun String?.ellipsize(n: Int) = this?.run { takeIf {length <= n } ?: "${take(20)}\u2026" }