package com.nobleworks_software.gmail_handler.internal

import com.nobleworks_software.gmail_handler.di.AppModule
import com.nobleworks_software.gmail_handler.emailRules
import space.traversal.kapsule.*

internal class EmailHandler(module: AppModule) : Injects<AppModule>
{
    private val messageSource by required { messageSource }

    private val messageDeleter by required { messageDeleter }

    private val taskCreator by required { taskCreator }

    private val transactionCreator by required { transactionCreator }

    init { inject(module) }

    fun process()
    {
        messageSource()
                .map { EmailHandlerDSL(it) }
                .onEach(emailRules)
                .onEach { taskCreator(it.tasks) }
                .onEach { transactionCreator(it.transactions) }
                .filter { it.delete }
                .map { it.message }
                .messageDeleter()
    }
}

