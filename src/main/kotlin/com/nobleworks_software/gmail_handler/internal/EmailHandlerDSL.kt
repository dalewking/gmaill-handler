package com.nobleworks_software.gmail_handler.internal

import com.nobleworks_software.gmail_handler.DSL
import com.nobleworks_software.gmail_handler.TaskBuilder
import com.nobleworks_software.ynab.client.model.SaveTransaction
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.mail.Message

class EmailHandlerDSL(override val message: Message): DSL
{
    val tasks = mutableListOf<TaskBuilder>()

    val transactions = mutableListOf<SaveTransaction>()

    internal var delete = false

    override val msgDate: LocalDate by lazy()
    {
        Instant.ofEpochMilli(message.sentDate.time).atZone(ZoneId.systemDefault()).toLocalDate()
    }

    override val msgDateTime: LocalDateTime by lazy()
    {
        Instant.ofEpochMilli(message.sentDate.time).atZone(ZoneId.systemDefault()).toLocalDateTime()
    }

    override val month: String by lazy { DateTimeFormatter.ofPattern("MMMM").format(msgDate) }

    override val year: String by lazy { DateTimeFormatter.ofPattern("YYYY").format(msgDate) }

    override val messageText: String by lazy { message.getText() ?: message.getText(true) ?: "" }

    override fun DSL?.hasSubject(regex: String)
            = this?.takeIf { message.subject.contains(Regex(regex.trim(), RegexOption.IGNORE_CASE)) }

    override fun DSL?.isFrom(regex: String)
            = this?.takeIf { message.from.any { it.toString().contains(Regex(regex.trim(), RegexOption.IGNORE_CASE)) } }

    override fun DSL?.bodyContains(regex: String)
            = this?.takeIf { messageText.contains(Regex(regex)) }

    override fun DSL?.makeTask(config: TaskBuilder.() -> Unit)
            = this?.apply { tasks += TaskBuilder(this).apply(config) }

    override fun DSL?.makeBudgetTransaction(transactionBuilder: DSL.() -> SaveTransaction?) : DSL?
        = this?.run{ transactionBuilder()?.apply { transactions += this }.let { this } }

    override fun DSL?.deleteIt() = this?.apply{ delete = true }
}