@file:JvmName("Main")

package com.nobleworks_software.gmail_handler

import com.nobleworks_software.gmail_handler.di.AppModule
import com.nobleworks_software.gmail_handler.internal.EmailHandler

fun main(args: Array<String>)
{
    EmailHandler(AppModule()).process()
}
