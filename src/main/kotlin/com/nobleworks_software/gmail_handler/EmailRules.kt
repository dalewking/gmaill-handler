package com.nobleworks_software.gmail_handler

import com.github.debop.javatimes.minutes
import com.nobleworks_software.ynab.client.model.SaveTransaction
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Entities
import org.xml.sax.InputSource
import java.io.StringReader
import java.math.BigDecimal
import java.time.format.DateTimeFormatter
import java.util.*
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

val String.toMilUnits: Long get()
    = (BigDecimal(this) * BigDecimal(1000)).toLong()

val emailRules: DSL.() -> Unit =
{
    val ynabFreedomVisa = UUID.fromString("d4c907f6-a30b-4715-955b-2228d9e44c38")
    val chaseChecking = UUID.fromString("9ef377cc-ac4b-4c28-b423-e25a03568e2e")
    val lastMonth = msgDate.minusMonths(1).format(DateTimeFormatter.ofPattern("MMMM YYYY"))

    isFrom("bursar@bsu.edu").hasSubject("Ball State Student eBill is now available")
                .makeTask()
                {
                    name = "Download and file $month $year Ball State Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    energy = Energy.LOW
                    dueDate = startDate?.run { withDayOfMonth(lengthOfMonth()) }
                    starred = true
                    estimate = 5.minutes
                    notes = "https://commerce.cashnet.com/cashnetg/selfserve/eBillLogin.aspx?client=BALL_PROD&LT=P \n\n$notes"
                }
                .makeTask()
                {
                    name = "Pay $month $year Ball State Bill"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    energy = Energy.LOW
                    dueDate = startDate?.run { withDayOfMonth(lengthOfMonth()) }
                    starred = true
                    estimate = 5.minutes
                    notes = "https://commerce.cashnet.com/cashnetg/selfserve/eBillLogin.aspx?client=BALL_PROD&LT=P  \n\n$notes"
                }
                .deleteIt()

    isFrom("vectren.com").hasSubject("Your Vectren bill is available to view online")
            .makeTask()
            {
                name = "Download and file $month $year Vectren Statement"
                folder = "Household"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 5.minutes
                notes = "https://www.vectren.com/ \n\n$notes"
            }
            .deleteIt()

    isFrom("FifthThirdBank@53.com").hasSubject("Account Document Available x7771")
            .makeTask()
            {
                name = "Download and file $month $year 5/3 Statement"
                folder = "Household"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 5.minutes
                notes = "https://www.53.com/ \n\n$notes"
            }
            .makeTask()
            {
                name = "Reconcile mortgage account in YNAB"
                folder = "Household"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 5.minutes
                notes = "https://www.53.com"
            }
            .deleteIt()

    isFrom("t-mobile@digital-delivery.com").hasSubject("Your T-Mobile bill is ready to view")
                .makeTask()
                {
                    name = "Download and file $month $year T-Mobile Statement"
                    folder = "Household"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://account.t-mobile.com/ \n\n$notes"
                }
                .deleteIt()

    isFrom("Myaccount@spectrumemails.com").hasSubject("Your statement is ready")
                .makeTask()
                {
                    name = "Download and file $month $year Spectrum Statement"
                    folder = "Household"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    startDate = startDate?.plusDays(2)
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://www.spectrum.net/ \n\n$notes"
                }
                .deleteIt()

    isFrom("fidelity.com").hasSubject("New statement available")
                .makeTask()
                {
                    name = "Download and file $lastMonth $year Fidelity Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://click.fidelityinvestments.com/?qs=42c6f2377af03d4a19a2fd3762f04b58dec5ce3998b35bb2fa587b8977ff93b77c1b0d1dd5a71c136ba5dd65d7952b55 \n\n$notes"
                }
                .deleteIt()

    isFrom("CITBank@alerts.citbank.com").hasSubject("Online Banking Alert - New Bank Statement Available")
            .makeTask()
            {
                name = "Download and file $lastMonth $year CIT Bank Statement"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 5.minutes
                notes = "https://BankOnCIT.com \n\n$notes"
            }
            .makeTask()
            {
                name = "Update tax planner with $lastMonth CIT Bank interest"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 3.minutes
            }
            .makeTask()
            {
                name = "Enter $lastMonth CIT Bank interest in YNAB"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(30)
                estimate = 3.minutes
            }
            .deleteIt()

    isFrom("client@notifications.tdameritrade.com").hasSubject("Your new TD Ameritrade statement is available")
                .makeTask()
                {
                    name = "Download and file Amanda's $lastMonth ESA Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://www.tdameritrade.com/home.page \n\n$notes"
                }
                .deleteIt()

    isFrom("no-reply@invoicecloud.net").hasSubject("City of Carmel Utilities Invoice")
                .makeTask()
                {
                    name = "Download and file $month $year Carmel Utilities Statement"
                    folder = "Household"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://www.invoicecloud.com/carmelin \n\n$notes"
                }
                .deleteIt()

    isFrom("DukeEnergyPaperlessBilling@duke-energy.com").hasSubject("Your Duke Energy statement is ready")
                .makeTask()
                {
                    name = "Download and file $month $year Duke Energy Statement"
                    folder = "Household"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "https://www.duke-energy.com/home \n\n$notes"
                }
                .deleteIt()

    isFrom("no-reply@alertsp.chase.com").hasSubject("Your deposit statement is available")
                .makeTask()
                {
                    name = "Download and file $month Chase Checking Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 5.minutes
                    notes = "http://chase.com/ \n\n$notes"
                }
                .deleteIt()

    isFrom("My-edelivery@centier.com").hasSubject("Centier Bank - You have a new eStatement for account")
                .makeTask()
                {
                    name = "Download and file $lastMonth Centier Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 3.minutes
                    notes = "https://www.myriadsystems.com/estatements/login.aspx?cid=centier \n\n$notes"
                }
                .deleteIt()

    isFrom("no-reply@alertsp.chase.com").hasSubject("Your credit card statement is ready")
                .makeTask()
                {
                    name = "Download and file ${month} $year Chase Freedom Unlimited Card Statement"
                    folder = "Finances"
                    tag = "@Computer"
                    context = "Computer"
                    starred = true
                    energy = Energy.LOW
                    dueDate = startDate?.plusDays(30)
                    estimate = 3.minutes
                    notes = "https://www.chase.com \n\n$notes"
                }
                .deleteIt()

    isFrom("MyPay@coxinc.com")
            .hasSubject("Paycheck Available to View")
            .makeTask()
            {
                val paycheckDate = msgDate
                        .plusDays(2)
                        .format(DateTimeFormatter.ofPattern("M-dd"))

                name = "Download and file $paycheckDate Paycheck"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.LOW
                dueDate = startDate?.plusDays(2)
                estimate = 4.minutes
                notes = "https://insidecox.coxenterprises.com/webcenter/portal/INSIDECOX/pages_mypay \n\n$notes"
            }
            .makeTask()
            {
                val paycheckDate = msgDate
                        .plusDays(2)
                        .format(DateTimeFormatter.ofPattern("M-dd"))

                name = "Update Tax Planner with $paycheckDate NextGear Paycheck"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.MEDIUM
                dueDate = startDate?.plusDays(2)
                estimate = 6.minutes
                notes = "https://docs.google.com/spreadsheets/d/13EARwL-O9XqzOJnJJoqkiVp1EcgHS8TYO22A531Rkq0/edit#gid=1140113306 \n\n$notes"
            }
            .makeTask()
            {
                val paycheckDate = msgDate
                        .plusDays(2)
                        .format(DateTimeFormatter.ofPattern("M-dd"))

                name = "Update YNAB scheduled transaction with $paycheckDate NextGear Paycheck"
                folder = "Finances"
                tag = "@Computer"
                context = "Computer"
                starred = true
                energy = Energy.MEDIUM
                dueDate = startDate?.plusDays(2)
                estimate = 6.minutes
            }
            .deleteIt()

    isFrom("DoNotReply_US@mcdonalds.com")
            .hasSubject("Thanks for placing a mobile order!")
            .makeBudgetTransaction()
            {
                val amountXpath = "//td[normalize-space(text())='Approved Amount:']/ancestor::tr[1]/td[last()]/text()"

                val cleanText = Jsoup.parse(messageText
                        .replace("&nbsp;", " ")
                        .replace("&copy;", "©"))
                        .apply()
                        {
                            outputSettings().syntax(Document.OutputSettings.Syntax.xml)
                            outputSettings().escapeMode(Entities.EscapeMode.xhtml)
                            outputSettings().charset("UTF-8")
                        }
                        .html()

                val xpathFactory = XPathFactory.newInstance()
                val xpath = xpathFactory.newXPath()

                val mcDonaldsPayee = UUID.fromString("61227aa9-50d3-4f28-9fc9-2d6e618a1e54")
                val diningOutCategory = UUID.fromString("8c3cb0d4-cb38-4467-8d52-d5a3da110cb2")

                val source = InputSource(StringReader(cleanText))

                (xpath.evaluate(amountXpath, source, XPathConstants.STRING) as String?)
                        ?.takeIf { it.isNotEmpty() }
                        ?.toMilUnits
                        ?.let()
                        {
                            SaveTransaction(
                                    accountId = ynabFreedomVisa,
                                    date = msgDate,
                                    amount = -it,
                                    payeeName = "McDonald's",
                                    payeeId = mcDonaldsPayee,
                                    categoryId = diningOutCategory,
                                    approved = true
                            )
                        }
            }
            .deleteIt()

    isFrom("noreply@ministryforms.net")
            .hasSubject("Thank you for your Donation!")
            .makeBudgetTransaction()
            {
                val catcPayee = UUID.fromString("f9ed7aeb-d22b-483d-9bf2-6c8ffcdbf1c2")
                val titheCategory = UUID.fromString("2da41d24-0bb0-49b1-8d10-1d7d843b0fd1")

                Regex("Your donation in the amount of \\$(.+) has been received and is being processed")
                        .find(messageText)
                        ?.groups
                        ?.get(1)
                        ?.value
                        ?.run{ toMilUnits }
                        ?.let()
                        {
                            SaveTransaction(
                                accountId = ynabFreedomVisa,
                                date = msgDate,
                                amount = -it,
                                payeeName = "Church At the Crossing",
                                payeeId = catcPayee,
                                categoryId = titheCategory,
                                approved = true
                            )
                        }
            }
            .deleteIt()

//    isFrom("no-reply@alertsp.chase.com")
//            .hasSubject("Thank you for scheduling your online payment")
//            ?.run()
//            {
//                Regex("You scheduled your payment of \\$(.+) for your account ending in 9478")
//                        .find(messageText)
//                        ?.groups
//                        ?.get(1)
//                        ?.value
//                        ?.run{ toMilUnits }
//                        ?.let()
//                        {
//                            makeBudgetTransaction()
//                            {
//                                YnabTransactionData(
//                                        accountId = chaseChecking,
//                                        date = msgDate.format(DateTimeFormatter.ISO_DATE),
//                                        amount = -it,
//                                        payeeName = "Transfer : Freedom Visa",
//                                        payeeId = "2302edcd-0e42-4fac-b295-23876e562b37",
//                                        transfer_accountId = ynabFreedomVisa,
//                                        categoryId = null,
//                                        approved = true
//                                )
//                            }
//                            .makeBudgetTransaction()
//                            {
//                                YnabTransactionData(
//                                        accountId = ynabFreedomVisa,
//                                        date = msgDate.format(DateTimeFormatter.ISO_DATE),
//                                        amount = it,
//                                        payeeName = "Transfer : Chase",
//                                        payeeId = "a735425f-59aa-43c2-8cdb-1cf8285c7bb4",
//                                        transfer_accountId = chaseChecking,
//                                        categoryId = null,
//                                        approved = true
//                                )
//                            }
//                        }
//            }
//            .deleteIt()
}