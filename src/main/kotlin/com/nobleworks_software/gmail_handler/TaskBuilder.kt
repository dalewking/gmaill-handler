package com.nobleworks_software.gmail_handler

import com.nobleworks_software.gmail_handler.internal.ellipsize
import java.time.Duration
import java.time.Instant
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

enum class Priority(val representation: String?)
{
    NONE(null), LOW("!"), MEDIUM("!!"), HIGH("!!!")
}

enum class Energy(val representation: Int?)
{
    NONE(null), LOW(1), MEDIUM(2), HIGH(3)
}

data class TaskBuilder(
        var name: String = "",
        var folder: String? = null,
        var tag: String? = null,
        var context: String? = null,
        var starred: Boolean = false,
        var energy: Energy = Energy.NONE,
        var startDate: LocalDate? = null,
        var dueDate: LocalDate? = null,
        var estimate: Duration? = null,
        var repeat: String? = null,
        var notes: String? = null)
{
    constructor(handler: DSL)
            : this(name = handler.message.subject, startDate = handler.msgDate, notes = handler.messageText)

    fun toNirvanaTask() : Map<String, Any?>
    {
        val timestamp = Instant.now().epochSecond
        val dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd")

        return mapOf(
                "method" to "task.save",
                "id" to UUID.randomUUID().toString(),
                "type" to 0,
                "parentid" to "",
                "waitingfor" to "",
                "state" to (folder?.let { 1 } ?: 0),
                "completed" to 0,
                "cancelled" to 0,
                "seq" to 0,
                "seqt" to if(starred) timestamp else 0,
                "seqp" to 0,
                "name" to name,
                "tags" to "$folder,@${context?.toLowerCase() ?: ""}",
                "note" to notes,
                "ps" to 0,
                "etime" to estimate?.toMinutes()?.toString() ,
                "energy" to energy.representation,
                "startdate" to (startDate?.let(dateFormat::format) ?: ""),
                "duedate" to (dueDate?.let(dateFormat::format) ?: ""),
                "recurring" to "",
                "_name" to timestamp,
                "_type" to timestamp,
                "_tags" to timestamp,
                "_parentid" to timestamp,
                "_note" to timestamp,
                "_waitingfor" to timestamp,
                "_state" to timestamp,
                "_completed" to timestamp,
                "_cancelled" to timestamp,
                "_seq" to timestamp,
                "_seqt" to timestamp,
                "_seqp" to timestamp,
                "_ps" to timestamp,
                "_etime" to timestamp,
                "_energy" to timestamp,
                "_startdate" to timestamp,
                "_duedate" to timestamp,
                "_recurring" to timestamp
        )
    }

    override fun toString(): String
    {
        return "TaskBuilder(name='$name', folder=$folder, tag=$tag, context=$context, starred=$starred, " +
                "startDate=$startDate, dueDate=$dueDate, estimate=$estimate, repeat=$repeat, " +
                "notes=${notes.ellipsize(20)})"
    }
}
